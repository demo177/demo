#!/bin/sh

# 通过Dockerfile生成镜像
docker build -t springbootdemo .

# 停掉并删除老容器1
docker stop springbootdemo1 & docker rm -f springbootdemo1

# 启动容器1，并把本机8000端口映射到容器8000端口
docker run -d -p 8000:8080 --name springbootdemo1 springbootdemo

# 判断容器1是ok的
healthy_check_flag=''
while :
do
    healthy_check_flag=`curl 127.0.0.1:8000/healthy`
    if [[ $healthy_check_flag = "healthy" ]]; then
        echo "yes"
        break
    fi
    sleep 1s
done

# 容器一已启动，3s后启动容器二
echo "容器一已启动，3s后启动容器二..."
sleep 3s

# 停掉并删除老容器2
docker stop springbootdemo2 & docker rm -f springbootdemo2

# 启动容器2，并把本机8001端口映射到容器8000端口
docker run -d -p 8001:8080 --name springbootdemo2 springbootdemo

# 删除 <none> 镜像
docker rmi $(docker images | grep "none" | awk '{print $3}')
