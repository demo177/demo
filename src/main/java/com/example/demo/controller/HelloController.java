package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("")
    public String hello() {
        // sudo docker login --username=育才的域名世界 registry.cn-hangzhou.aliyuncs.com
        // YANG1516
        // docker pull registry.cn-hangzhou.aliyuncs.com/ayyc/demo:[镜像版本号]

        //登录阿里云Docker Registry
        //$ sudo docker login --username=è²æçååä¸ç registry.cn-hangzhou.aliyuncs.com
        //用于登录的用户名为阿里云账号全名，密码为开通服务时设置的密码。
        //
        //您可以在访问凭证页面修改凭证密码。
        //
        //2. 从Registry中拉取镜像
        //$ sudo docker pull registry.cn-hangzhou.aliyuncs.com/ayyc/demo:[镜像版本号]
        //3. 将镜像推送到Registry
        //$ sudo docker login --username=è²æçååä¸ç registry.cn-hangzhou.aliyuncs.com
        //$ sudo docker tag [ImageId] registry.cn-hangzhou.aliyuncs.com/ayyc/demo:[镜像版本号]
        //$ sudo docker push registry.cn-hangzhou.aliyuncs.com/ayyc/demo:[镜像版本号]
        //请根据实际镜像信息替换示例中的[ImageId]和[镜像版本号]参数。
        //
        //4. 选择合适的镜像仓库地址
        //从ECS推送镜像时，可以选择使用镜像仓库内网地址。推送速度将得到提升并且将不会损耗您的公网流量。
        //
        //如果您使用的机器位于VPC网络，请使用 registry-vpc.cn-hangzhou.aliyuncs.com 作为Registry的域名登录。
        //
        //5. 示例
        //使用"docker tag"命令重命名镜像，并将它通过专有网络地址推送至Registry。
        //
        //$ sudo docker images
        //REPOSITORY                                                         TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
        //registry.aliyuncs.com/acs/agent                                    0.7-dfb6816         37bb9c63c8b2        7 days ago          37.89 MB
        //$ sudo docker tag 37bb9c63c8b2 registry-vpc.cn-hangzhou.aliyuncs.com/acs/agent:0.7-dfb6816
        //使用 "docker push" 命令将该镜像推送至远程。
        //
        //$ sudo docker push registry-vpc.cn-hangzhou.aliyuncs.com/acs/agent:0.7-dfb6816
        System.out.println("ssssssssssssssss log");
        return "hello, 555!";
    }

    @GetMapping("/healthy")
    public String healthy() {
        return "healthy";
    }

}
